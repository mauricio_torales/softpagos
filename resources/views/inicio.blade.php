<!DOCTYPE html>
<head>
   <meta charset="utf-8"/>
   <title>Soft Pagos| Pagos online sin complicaciones</title>
   <meta property="og:type" content="website"/>
   <meta content="summary_large_image" name="twitter:card"/>
   <meta content="width=device-width, initial-scale=1" name="viewport"/>
   <link href="css/landing.css" rel="stylesheet" type="text/css"/>
   <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
   <style>
       .desk-logo{
           width: 100px
       }
      .input {
      /* Remove First */
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      }
      .input:valid {
      border-color: #35375E;
      }
      .input:invalid {
      color: #A53050;
      }
      .button:focus {
      outline: 1px dashed #DEC1AD;
      -moz-outline-radius: 57px;
      }
      .readmore-link:focus {
      outline: 1px dashed #DEC1AD;
      -moz-outline-radius: 57px;
      }
   </style>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/110/three.min.js"></script>
   <style>
      .w-slider-dot {
      top: 3px;
      width: 6px;
      height: 6px;
      background: #C2C3CF;
      }
      .w-slider-dot.w-active {
      transform: scale(1.67);
      background: #35375E;
      }
      .input {
      /* Remove First */
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      }
      .input:valid {
      border-color: #35375E;
      }
      .input:invalid {
      color: #A53050;
      }
      .button:focus {
      outline: 1px dashed #DEC1AD;
      -moz-outline-radius: 57px;
      }
      .readmore-link:focus {
      outline: 1px dashed #DEC1AD;
      -moz-outline-radius: 57px;
      }
      #preloader {
      width: 37.5vh;
      height: 37.5vh;
      position: absolute;
      z-index: 1;
      }
      #preloader.in circle {
      stroke-width: 20;
      animation-delay: 200ms;
      transition: stroke-width 200ms 200ms;
      }
      #preloader.out circle {
      transition: stroke-width 200ms;
      }
      #preloader circle {
      stroke-width: 0;
      transform-origin: 275px;
      animation-duration: 2000ms;
      animation-timing-function: linear;
      animation-iteration-count: infinite;
      animation-fill-mode: both;
      animation-play-state: paused;
      }
      #preloader.in circle,
      #preloader.out circle {
      animation-play-state: running;
      }
      #preloader #preloader-circle-0 {
      animation-name: in-0;
      }
      #preloader #preloader-circle-1 {
      animation-name: in-1;
      }
      #preloader #preloader-circle-2 {
      animation-name: in-2;
      }
      #preloader #preloader-circle-3 {
      animation-name: in-3;
      }
      #preloader #preloader-circle-4 {
      animation-name: in-4;
      }
      .nav.w-nav,
      .mobile-menu {
      transition: transform 800ms;
      }
      .loading > .nav.w-nav {
      transform: translateY(-100%);
      }
      .loading > .mobile-menu {
      transform: translateY(-125%);
      }
      .section-home.hero > #to-colony-section,
      .section-home.hero > #to-insights-section {
      transition: transform 900ms;
      }
      .loading .section-home.hero > #to-colony-section,
      .loading .section-home.hero > #to-insights-section {
      transform: translateY(150%);
      }
      .w-slider-dot {
      top: 3px;
      width: 6px;
      height: 6px;
      background: #C2C3CF;
      }
      .w-slider-dot.w-active {
      transform: scale(1.67);
      background: #35375E;
      }
      #animation canvas {
      width: 100vw;
      height: 100vh;
      min-width: 360px;
      }
      .section-home {
      visibility: hidden;
      transition: visibility 0ms 600ms;
      }
      .section-home.visible {
      z-index: 1;
      visibility: visible;
      transition: visibility 0ms 0ms;
      }
      .section-home .hero-heading {
      opacity: 0;
      animation-duration: 600ms;
      animation-iteration-count: 1;
      animation-fill-mode: both;
      }
      .section-home .sub-tittle,
      .section-home #slider-wrapper {
      opacity: 0;
      animation-duration: 600ms;
      animation-iteration-count: 1;
      animation-fill-mode: both;
      }
      #companies-link:not(:hover),
      #companies-link:not(:hover) .arrow-right,
      #Individuals-link:not(:hover),
      #Individuals-link:not(:hover) .arrow-left {
      transition: transform 1000ms;
      }
      #companies-link {
      transform: translateX(400%);
      }
      #Individuals-link {
      transform: translateX(-400%);
      }
      #companies-link .arrow-right {
      transform: translateX(-300%);
      }
      #Individuals-link .arrow-left {
      transform: translateX(300%);
      }
      #home-section.visible #companies-link,
      #home-section.visible #companies-link .arrow-right,
      #home-section.visible #Individuals-link,
      #home-section.visible #Individuals-link .arrow-left {
      transform: translateX(0);
      }
      #to-home-section {
      transition: opacity 800ms;
      }
      #to-colony-section,
      #to-insights-section,
      #to-home-section {
      opacity: 0;
      visibility: hidden;
      }
      .section-home.visible #to-colony-section,
      .section-home.visible #to-insights-section,
      .section-home.visible #to-home-section {
      opacity: 1;
      visibility: visible;
      }
      @keyframes in-0 {
      0% { transform: rotate(-45deg) }
      to { transform: rotate(-405deg) }
      }
      @keyframes in-1 {
      0% { transform: rotate(70deg) }
      to { transform: rotate(430deg) }
      }
      @keyframes in-2 {
      0% { transform: rotate(70deg) }
      to { transform: rotate(-290deg) }
      }
      @keyframes in-3 {
      0% { transform: rotate(200deg) }
      to { transform: rotate(-160deg) }
      }
      @keyframes in-4 {
      0% { transform: rotate(-20deg) }
      to { transform: rotate(340deg) }
      }
      @keyframes heading-in-up {
      0% {
      opacity: 0;
      line-height: 100%;
      transform: translateY(100%);
      }
      to {
      opacity: 1;
      line-height: 140%;
      transform: translateY(0%);
      }
      }
      @keyframes heading-in-down {
      0% {
      opacity: 0;
      line-height: 100%;
      transform: translateY(-180%);
      }
      to {
      opacity: 1;
      line-height: 140%;
      transform: translateY(0%);
      }
      }
      @keyframes heading-out-up {
      0% {
      opacity: 1;
      line-height: 140%;
      transform: translateY(0%);
      }
      to {
      opacity: 0;
      line-height: 100%;
      transform: translateY(-180%);
      }
      }
      @keyframes heading-out-down {
      0% {
      opacity: 1;
      line-height: 140%;
      transform: translateY(0%);
      }
      to {
      opacity: 0;
      line-height: 100%;
      transform: translateY(100%);
      }
      }
      @keyframes paragraph-in-up {
      0% {
      opacity: 0;
      transform: translateY(100%);
      }
      to {
      opacity: 1;
      transform: translateY(0%);
      }
      }
      @keyframes paragraph-in-down {
      0% {
      opacity: 0;
      transform: translateY(-100%);
      }
      to {
      opacity: 1;
      transform: translateY(0%);
      }
      }
      @keyframes paragraph-out-up {
      0% {
      opacity: 1;
      transform: translateY(0%);
      }
      to {
      opacity: 0;
      transform: translateY(-100%);
      }
      }
      @keyframes paragraph-out-down {
      0% {
      opacity: 1;
      transform: translateY(0%);
      }
      to {
      opacity: 0;
      transform: translateY(100%);
      }
      }
      @keyframes slide-in {
      0% {
      opacity: 0;
      transform: translateY(50%);
      }
      to {
      opacity: 1;
      transform: translateY(0%);
      }
      }
      @keyframes slide-out {
      0% {
      opacity: 1;
      transform: translateY(0%);
      }
      to {
      opacity: 0;
      transform: translateY(50%);
      }
      }
   </style>
</head>
<body class="loading">
   <div class="mobile-menu">
      <div class="nav-bg"></div>
      <div class="nav-container">
         <a href="index.html" aria-current="page" class="logo w-nav-brand w--current">

            <div class="mobile-logo"><img src="./img/logo.svg" alt="" class="mobile-logo__white"/><img src="./img/logo.svg" alt="" class="mobile-logo__dark"/></div>
         </a>
         <div data-w-id="037184f2-1c5f-31b3-f10f-1437423b0871" class="m-menu-button hide">
            <div class="nav-button__holder">
               <div class="nav-button-line"></div>
               <div class="nav-button-line__shortest"></div>
               <div class="nav-button-line__mid"></div>
            </div>
         </div>
         <div class="nav-mobile-holder"><a data-w-id="037184f2-1c5f-31b3-f10f-1437423b0877" href="services.html" class="nav--link">Services</a><a data-w-id="037184f2-1c5f-31b3-f10f-1437423b0879" href="the-colony.html" class="nav--link">The Colony</a><a data-w-id="037184f2-1c5f-31b3-f10f-1437423b087b" href="events.html" class="nav--link">Events</a><a data-w-id="037184f2-1c5f-31b3-f10f-1437423b087d" href="principles.html" class="nav--link">Principles</a><a data-w-id="037184f2-1c5f-31b3-f10f-1437423b087f" href="team.html" class="nav--link">Team</a></div>
      </div>
   </div>
   <div data-animation="default" class="nav w-nav" data-easing2="ease-in-sine" data-easing="ease-in-sine" data-collapse="medium" data-w-id="cb309eef-9c2b-ea48-90b6-ec5ce583ed0d" role="banner" data-no-scroll="1" data-duration="200" data-doc-height="1">
      <div class="container nav-container w-container">
        <img src="./img/logo.svg" alt="" class="desk-logo"/>
         <a href="index.html" aria-current="page" class="logo w-nav-brand w--current">

            <div class="mobile-logo"><img src="./img/logo.svg" alt="" class="mobile-logo__white"/><img src="./img/logo.svg" alt="" class="mobile-logo__dark"/></div>
         </a>
         <nav role="navigation" class="nav-menu w-nav-menu">
            <div class="nav-mobile-holder">

                <a href="principles.html" class="nav--link w-nav-link">Ingresar</a>
                <a href="/registro" class="nav--link w-nav-link button " style="padding-top:5px;padding-bottom:5px;margin-top:10px ">Registrarme</a></div>
            <div data-delay="0" class="dropdown-lang hide w-dropdown">
               <div class="lang-toggle w-dropdown-toggle">
                  <div>En</div>
               </div>
               <nav class="lang-list w-dropdown-list"><a href="#" class="dropdown-link w-dropdown-link">Es</a><a href="#" class="dropdown-link w-dropdown-link">汉</a></nav>
            </div>
         </nav>
         <div class="menu-button w-nav-button">
            <div class="nav-button__holder">
               <div class="nav-button-line"></div>
               <div class="nav-button-line__shortest"></div>
               <div class="nav-button-line__mid"></div>
            </div>
         </div>
      </div>

   </div>
   <div id="application" class="application">
      <div id="animation" class="animation-box">
         <div class="html-embed w-embed">
            <img src="./img/loader.gif" alt="" id="preloader"/>
         </div>
      </div>
      <div id="home-section" class="section-home hero">
         <div id="w-node-df7bbc8dab0c-f4d6711d" class="centered-box main">
            <h2 class="hero-heading">SOFT PAGOS  es una plataforma que pone a disposición de tus clientes todas las formas de pago.</h2>
            <p class="sub-tittle hero">Podés realizar tus ventas online con tarjetas de crédito y débito, billeteras telefónicas, bocas de cobranza y cuentas bancarias.</p>
         </div>
         <div id="w-node-80ebe2d860c9-f4d6711d" class="link-box">
            <a id="Individuals-link" data-w-id="f961f203-8be9-d70b-a158-6053a3553ec9" href="services.html#individuals" class="switch-scroller-side w-inline-block">


            </a>
            <a id="companies-link" data-w-id="785af5c6-9d99-1471-4151-78f2932d1b97" href="services.html#companies" class="switch-scroller-side w-inline-block">


            </a>
         </div>
         <a id="to-colony-section" data-w-id="8b723a33-f40d-6974-e8d7-26ccdcbe84b9" href="#" class="switch-scroller w-node-26ccdcbe84b9-f4d6711d w-inline-block">
            <div><span>1</span>\<span>3</span></div>
            <div><span>Ver más</span></div>
            <div style="-webkit-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" class="arrow-down-changer">
               <div class="arrow-down">
                  <div style="opacity:0" class="arrow-downr-red"></div>
               </div>
            </div>
         </a>
      </div>
      <div id="about-section" class="section-home">
         <div id="w-node-0792399254d4-f4d6711d" class="centered-box main-colony">
            <h2 class="hero-heading">Entendemos el futuro del trabajo porque lo estamos creando.</h2>
            <p class="sub-tittle hero">Desde softworks creamos esta plataforma de servicio como principal impulso el enfoque a un futuro mas libre y sin cargos extras por tanto trabajo realizado con mucho esfuerzo.</p>
         </div>
         <a id="to-insights-section" data-w-id="28e27f78-8039-48c8-0db4-fcfe20c57e57" href="#" class="switch-scroller w-node-fcfe20c57e57-f4d6711d w-inline-block">
            <div>2\<span>3</span></div>
            <div><span>Plataformas</span></div>
            <div style="-webkit-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" class="arrow-down-changer">
               <div class="arrow-down">
                  <div style="opacity:0" class="arrow-downr-red"></div>
               </div>
            </div>
         </a>
      </div>
      <div id="slider-section" class="section-home slider-section-home">
         <div id="slider-wrapper" class="slider-wrapp w-node-c7693d491231-f4d6711d">
            <div class="top-text">Un simple servicio que lo tenga todo</div>
            <div data-animation="outin" data-nav-spacing="7" data-duration="500" data-infinite="1" class="main-slider w-slider">
               <div class="mask w-slider-mask">
                  <div class="w-slide">
                     <div class="w-dyn-list">
                        <div role="list" class="w-dyn-items">
                           <div role="listitem" class="main-insights-item w-dyn-item">
                              <h3>BANCARD</h3>
                              <div class="main-subinsights">Tarjetas de credito y debito</div>
                              <div class="insights-paragraph-wrapp">
                                  <p>Ofrece a tus clientes la posibilidad de realizar pagos seguros a travez de sus tarjetas de credito o debito, suscripciones o pagos ocasionales.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="w-slide">
                     <div class="w-dyn-list">
                        <div role="list" class="w-dyn-items">
                           <div role="listitem" class="main-insights-item w-dyn-item">
                              <h3>Tigo Money</h3>
                              <div class="main-subinsights">Billetera movil</div>
                              <div class="insights-paragraph-wrapp">
                                <p>Ofrece a tus clientes la posibilidad de realizar pagos seguros a travez de sus tarjetas de credito o debito, suscripciones o pagos ocasionales.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="w-slide">
                     <div class="w-dyn-list">
                        <div role="list" class="w-dyn-items">
                           <div role="listitem" class="main-insights-item w-dyn-item">
                              <h3>CIO</h3>
                              <div class="main-subinsights">Fortune 5, Boston</div>
                              <div class="insights-paragraph-wrapp">
                                 <p>The tech team&#x27;s productivity is even with our pre-covid productivity.  We were at 15% remote so we are starting to think of going 50-60% remote-- why spend millions on expensive real estate?  But even if we achieve increased remote work, people will want to come in from time-to-time so we have a Hotelling problem. People want to know if a desk or room has been cleaned since last use.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="w-slide">
                     <div class="w-dyn-list">
                        <div role="list" class="w-dyn-items">
                           <div role="listitem" class="main-insights-item w-dyn-item">
                              <h3>VP Design</h3>
                              <div class="main-subinsights">Public Technology Co, SF Bay Area</div>
                              <div class="insights-paragraph-wrapp">
                                 <p>Clients can&#x27;t focus for more than 90 minutes in remote design thinking sessions. We have to be way more clever and explicit with our prompt design and watch for muting.  We have to work a lot harder to moderate and document. The silver lining is getting to know people in more human ways.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="w-slide">
                     <div class="w-dyn-list">
                        <div role="list" class="w-dyn-items">
                           <div role="listitem" class="main-insights-item w-dyn-item">
                              <h3>CEO</h3>
                              <div class="main-subinsights">Startup, Shanghai and SF Bay Area</div>
                              <div class="insights-paragraph-wrapp">
                                 <p>There are no U.S. standards for consumer masks and there is a need for education. People don&#x27;t understand the difference between masks that protect users and those that protect others.  China has a consumer mask standard.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="w-slide">
                     <div class="w-dyn-list">
                        <div role="list" class="w-dyn-items">
                           <div role="listitem" class="main-insights-item w-dyn-item">
                              <h3>Head of Design</h3>
                              <div class="main-subinsights">Unicorn, SF Bay Area</div>
                              <div class="insights-paragraph-wrapp">
                                 <p>This generation of technology practitioners is the next generation manager and in no other industry does that cause a problem. Imagine a Rust programmer in 20 years managing a PhD in quantum computing without a clue about what that person is doing.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div id="w-slider-arrow-left" class="slider-arrow-left w-slider-arrow-left"><img src="https://assets.website-files.com/5e3acb08a305e2bb3bd6711c/5e565c2f817dd37d81044ff7_arrow-left-slider.svg" alt=""/></div>
               <div id="w-slider-arrow-right" class="slider-arrow-right w-slider-arrow-right"><img src="https://assets.website-files.com/5e3acb08a305e2bb3bd6711c/5e565c32d68568eb6ad807a6_arrow-right-slider.svg" alt=""/></div>
               <div class="slide-nav w-slider-nav w-round"></div>
               <a href="the-colony.html" class="button more-insights w-button">Registrame ahora</a>
            </div>
         </div>
         <a href="#" id="to-home-section" class="home-linked w-node-88d35ceca25f-f4d6711d">Inicio</a>
      </div>
   </div>
   <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js?site=5e3acb08a305e2bb3bd6711c" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script><script src="https://assets.website-files.com/5e3acb08a305e2bb3bd6711c/js/open-colony.d7ae030c5.js" type="text/javascript"></script><!--[if lte IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
   <script>
      // Detecting if it is an iOS device, true/false
        var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

        $(document).ready(function(){
          // Defining that "overlay" is the element that has a changing display value
          var overlay = document.querySelector('.w-nav-overlay');

          // Creating our mutation observer, which we attach to overlay later
          var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutationRecord) {

              // Checking if it's the style attribute got changed and if display value now set to 'none'?
              if( mutationRecord.attributeName === 'style' && window.getComputedStyle(overlay).getPropertyValue('display') !== 'none'){

                //Overlay's  display value is no longer 'none', now changing the "body" styles:
                if (iOS) {
                  // for iOS devices:
                  var x = $(window).scrollTop().toFixed()


                  $('body').css({'overflow': 'hidden',
                                 'position': 'fixed',
                                 'top' : '-' + x + 'px',
                                 'width': '100vw'});
                }
                // for all other devices:
                $('body').css('overflow', 'hidden');
              }
               //Overlay's  display value back to 'none' , now changing the "body" styles again:
               else {
                     if (iOS) {
                     //  for iOS devices:
                        var t = $('body').css('top').replace('-','').replace('px','')
                        $('body').css({'overflow': 'auto',
                                       'position': '',
                                       'width': '100vw'});
                        $('body').animate({scrollTop:t}, 0);
                     }
                    // for all other devices:
                    $('body').css('overflow', '');

              }

            });
          });
          // Attach the mutation observer to overlay, and only when attribute values change
          observer.observe(overlay, { attributes : true, attributeFilter : ['style']});

        });

   </script><script type="text/javascript" src="./js/landing.js"></script>
</body>

</html>
