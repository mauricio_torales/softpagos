<!DOCTYPE html><!-- Last Published: Thu Jul 30 2020 21:29:23 GMT+0000 (Coordinated Universal Time) -->
<html data-wf-domain="www.opencolony.com" data-wf-page="5e404c323201574704bedfa0" data-wf-site="5e3acb08a305e2bb3bd6711c">
   <!-- Mirrored from www.opencolony.com/services by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 06 Aug 2020 01:52:51 GMT -->
   <head>
      <meta charset="utf-8"/>
      <title>Soft Pagos | Registro</title>

      <meta property="og:type" content="website"/>

      <meta content="width=device-width, initial-scale=1" name="viewport"/>
      <link href="css/landing.css" rel="stylesheet" type="text/css"/>
      <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script><script type="text/javascript">WebFont.load({  google: {    families: ["Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Red Hat Display:regular,italic,500,500italic,700,700italic,900,900italic","Space Mono:regular,italic,700,700italic"]  }});</script><!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif]--><script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>

      <style>
           .desk-logo{
           width: 100px
       }
         .input {
         /* Remove First */
         -webkit-appearance: none;
         -moz-appearance: none;
         appearance: none;
         }
         .input:valid {
         border-color: #35375E;
         }
         .input:invalid {
         color: #A53050;
         }
         .button:focus {
         outline: 1px dashed #DEC1AD;
         -moz-outline-radius: 57px;
         }
         .readmore-link:focus {
         outline: 1px dashed #DEC1AD;
         -moz-outline-radius: 57px;
         }
      </style>
      <style>
         .event-image {
         display: block;
         border-radius: 50% 50% 50% 50% / 65% 55% 40% 30%;
         }
         .event-item:nth-child(2n) .event-image {
         display: block;
         border-radius: 50% 50% 50% 50% / 50% 50% 40% 40%;
         }
         .event-item:nth-child(3n) .event-image {
         display: block;
         border-radius: 50% 50% 50% 50% / 40% 40% 65% 35%;
         }
      </style>
   </head>
   <body>
    <div class="mobile-menu">
        <div class="nav-bg"></div>
        <div class="nav-container">
           <a href="index.html" aria-current="page" class="logo w-nav-brand w--current">

              <div class="mobile-logo"><img src="./img/logo.svg" alt="" class="mobile-logo__white"/><img src="./img/logo.svg" alt="" class="mobile-logo__dark"/></div>
           </a>
           <div data-w-id="037184f2-1c5f-31b3-f10f-1437423b0871" class="m-menu-button hide">
              <div class="nav-button__holder">
                 <div class="nav-button-line"></div>
                 <div class="nav-button-line__shortest"></div>
                 <div class="nav-button-line__mid"></div>
              </div>
           </div>
           <div class="nav-mobile-holder"><a data-w-id="037184f2-1c5f-31b3-f10f-1437423b0877" href="services.html" class="nav--link">Services</a><a data-w-id="037184f2-1c5f-31b3-f10f-1437423b0879" href="the-colony.html" class="nav--link">The Colony</a><a data-w-id="037184f2-1c5f-31b3-f10f-1437423b087b" href="events.html" class="nav--link">Events</a><a data-w-id="037184f2-1c5f-31b3-f10f-1437423b087d" href="principles.html" class="nav--link">Principles</a><a data-w-id="037184f2-1c5f-31b3-f10f-1437423b087f" href="team.html" class="nav--link">Team</a></div>
        </div>
     </div>
     <div data-animation="default" class="nav w-nav" data-easing2="ease-in-sine" data-easing="ease-in-sine" data-collapse="medium" data-w-id="cb309eef-9c2b-ea48-90b6-ec5ce583ed0d" role="banner" data-no-scroll="1" data-duration="200" data-doc-height="1">
        <div class="container nav-container w-container">
          <img src="./img/logo.svg" alt="" class="desk-logo"/>
           <a href="index.html" aria-current="page" class="logo w-nav-brand w--current">

              <div class="mobile-logo"><img src="./img/logo.svg" alt="" class="mobile-logo__white"/><img src="./img/logo.svg" alt="" class="mobile-logo__dark"/></div>
           </a>
           <nav role="navigation" class="nav-menu w-nav-menu">
              <div class="nav-mobile-holder">

                  <a href="principles.html" class="nav--link w-nav-link">Ingresar</a>

              <div data-delay="0" class="dropdown-lang hide w-dropdown">
                 <div class="lang-toggle w-dropdown-toggle">
                    <div>En</div>
                 </div>
                 <nav class="lang-list w-dropdown-list"><a href="#" class="dropdown-link w-dropdown-link">Es</a><a href="#" class="dropdown-link w-dropdown-link">汉</a></nav>
              </div>
           </nav>
           <div class="menu-button w-nav-button">
              <div class="nav-button__holder">
                 <div class="nav-button-line"></div>
                 <div class="nav-button-line__shortest"></div>
                 <div class="nav-button-line__mid"></div>
              </div>
           </div>
        </div>

     </div>
      <a data-w-id="41ba4d3f-3c9a-3cb9-ae17-cac95d800eed" href="index.html" class="back-link w-inline-block">
         <div style="-webkit-transform:translate3d(0PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:1" class="link-text">
            <div style="-webkit-transform:translate3d(0PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" class="arrow-left">
               <div style="opacity:0" class="arrow-left-red"></div>
            </div>
            <div>Go Back</div>
         </div>
         <div style="-webkit-transform:translate3d(0PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="link-circle">
            <div class="arrow-left-copy">
               <div style="opacity:0" class="arrow-left-red"></div>
            </div>
         </div>
      </a>

      <div class="section form-section">
         <div class="container w-container">
            <div class="form-heading">
               <h2 class="form-title">Hola.<br/>Empecemos con crear su cuenta!</h2>
            </div>
            <div class="form-block w-form">
               <form id="email-form" name="email-form" data-name="Email Form" method="post" action="https://opencolony.us4.list-manage.com/subscribe/post?u=904eed5e7497c3e481ed2d91b&amp;amp;id=a85ad5089d" class="form">
                  <div class="input-block"><label for="EMAIL" class="label">Su Email</label><input type="email" class="input w-input" maxlength="256" name="EMAIL" data-name="*|EMAIL|*" placeholder="ejemplo@gmail.com" id="EMAIL" required=""/></div>
                  <div class="input-block"><label for="MESSAGE" class="label">Nombre y Apellido</label><input type="text" class="input w-input" maxlength="256" name="MESSAGE" data-name="MESSAGE" placeholder="String" id="MESSAGE" required=""/></div>
                  <div class="input-block"><label for="MESSAGE" class="label">Contrasena</label><input type="password" class="input w-input" maxlength="256" name="MESSAGE" data-name="MESSAGE" placeholder="String" id="MESSAGE" required=""/></div>
                  <div class="hide w-embed">
                     <ul>
                        <li><input type="checkbox" value="1" name="group[66843][1]" id="mce-group[66843]-66843-0" checked><label for="mce-group[66843]-66843-0">Standard Form</label></li>
                     </ul>
                  </div>
                  <div class="div-block"><input type="submit" value="Registrarme" data-wait="Please wait..." class="submitt-btn w-button"/></div>
               </form>
               <div class="succes-form w-form-done">
                  <div>Thanks! Your email has been sent.  We will respond shortly.</div>
                  <a id="refresh-form" href="#" class="submitt-btn refresh w-button">Send New Message</a>
               </div>
               <div class="w-form-fail">
                  <div>Oops! Something went wrong while submitting the form.</div>
               </div>
            </div>
         </div>
      </div>
      <div class="section-footer services">
         <div class="container w-container">
            <div class="footer-flex">
               <div class="footer-links"><a href="contact-us.html#back" class="footer-link">Contactenos</a><a href="terms-of-use.html#back-terms" class="footer-link">Terminos y condiciones</a><a href="privacy-policy.html#back-privacy" class="footer-link">Politica de privacidad</a></div>
               <div class="flex-copywrite">
                
                  <div class="footer-link"> SoftWorsk Py © 2020</div>
               </div>
            </div>
         </div>
      </div>
      <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js?site=5e3acb08a305e2bb3bd6711c" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script><script src="https://assets.website-files.com/5e3acb08a305e2bb3bd6711c/js/open-colony.d7ae030c5.js" type="text/javascript"></script><!--[if lte IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
      <script>
         // Detecting if it is an iOS device, true/false
           var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

           $(document).ready(function(){
             // Defining that "overlay" is the element that has a changing display value
             var overlay = document.querySelector('.w-nav-overlay');

             // Creating our mutation observer, which we attach to overlay later
             var observer = new MutationObserver(function(mutations) {
               mutations.forEach(function(mutationRecord) {

                 // Checking if it's the style attribute got changed and if display value now set to 'none'?
                 if( mutationRecord.attributeName === 'style' && window.getComputedStyle(overlay).getPropertyValue('display') !== 'none'){

                   //Overlay's  display value is no longer 'none', now changing the "body" styles:
                   if (iOS) {
                     // for iOS devices:
                     var x = $(window).scrollTop().toFixed()


                     $('body').css({'overflow': 'hidden',
                                    'position': 'fixed',
                                    'top' : '-' + x + 'px',
                                    'width': '100vw'});
                   }
                   // for all other devices:
                   $('body').css('overflow', 'hidden');
                 }
                  //Overlay's  display value back to 'none' , now changing the "body" styles again:
                  else {
                        if (iOS) {
                        //  for iOS devices:
                           var t = $('body').css('top').replace('-','').replace('px','')
                           $('body').css({'overflow': 'auto',
                                          'position': '',
                                          'width': '100vw'});
                           $('body').animate({scrollTop:t}, 0);
                        }
                       // for all other devices:
                       $('body').css('overflow', '');

                 }

               });
             });
             // Attach the mutation observer to overlay, and only when attribute values change
             observer.observe(overlay, { attributes : true, attributeFilter : ['style']});

           });

      </script><script>
         // Check if url contains needed parameters
         if (window.location.href.indexOf("services#companies") > -1) {
           document.querySelector('.back-link').style.display = 'flex';
         }
         if (window.location.href.indexOf("services#individuals") > -1) {
           document.querySelector('.back-link').style.display = 'flex';
         }
      </script>
   </body>

</html>
